import { Component } from '@angular/core';

import { fade_fx } from './animation.component';

@Component(
{
	selector: 'home',
	templateUrl: '../views/home.html',
	animations: [fade_fx]
})

export class HomeComponent
{
	public title: string;
	public slogan: string;
	public know_our_animals: string;
	public meet_our_keepers: string;
	public contact_us: string;

	constructor()
	{
		this.title = 'Bienvenido a nuestro zoo!';
		this.slogan = 'Animales y diversión';
		this.know_our_animals = 'Cientos de animales!';
		this.meet_our_keepers = 'Nuestros cuidadores te orientarán';
		this.contact_us = 'Cuentanos tu experiencia';
	}
}
