Desplegar:
---------
Para desplegar en localhost mediante tunnel:
$ ngrok http 4444 -host-header="localhost:4444"


Desplegar Front
----------------
	1.  Instalar angular versión 5
	$ npm install -g @angular/cli@1.7.1

	2. Instalar node
	npm install

	3. Arrancar
	npm start o ng server --port 4444

Desplegar backend:
-----------
	1. Instalar node
	npm install

	2. Arrancar
	npm start 
    
test
----
david@ngzoo.com       --> admin/  12345678
davidkeeper@ngzoo.com --> keeper  12345678

TODO:
-----
25/06/2021 Rev final:
- Background-position de fondo cabecera.                      - OK
- No va a la página de detalles de animale en parte usuario.  - OK
- Revisar ruta de detalles de animal: el enlace funciona si se sube a upload/animal/fotos - OK
- Fallo alineación botones                                    - OK
- Botón Añadir en gestión                                     - OK
- No upload images
20/06/2021
- Controlador animals fallo al subir imagénes. -OK
18/06/2021
- No reconoce token de usuario. Deprecated - OK
16/06/2021
- Revisar backend. No hace login         - OK
15/06/2021
- No guarda imagen en base de datos       - OK



# Angular5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
