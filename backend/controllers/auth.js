'use strict';

let bcrypt = require('bcrypt-nodejs');

let User = require('../models/user');

let jwt = require('../services/jwt');

function login(req, res)
{
	let params = req.body;

	if(params.email != undefined && params.password != undefined)
	{
		User.findOne({ email: params.email.toLowerCase() },
			(err, user) =>
			{
				if(err)
				{
					res.status(500).send({ 
					message: 'Error consultando la base de datos'});
				} else {

					if (user)
					{
						bcrypt.compare(params.password, user.password, (err, check) =>
						{
							if (check)
							{
								if (params.gettoken == 'true')
								{
									res.status(200).send({ token: jwt.createToken(user) });
								} else {

									res.status(200).send(
									{ user: user }
									);
								}
							} else {

								res.status(500).send(
								{ message: 'El usuario o contraseña es incorrecto' }
								);
							}
						});
					} else {

						res.status(500).send(
						{ message: 'Usuario no registrado en la base de datos' }
						);
					}
				}
			});
	} else {

		res.status(400).send(
		{ message: 'No se han recibido datos.'}
		);
	}
}

module.exports = { login };