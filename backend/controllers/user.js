'use strict';

let bcrypt = require('bcrypt-nodejs');

let User = require('../models/user');

function create(req, res)
{
	let user = new User;
	let params = req.body;

	if(params)
	{
		User.findOne({ email: params.email.toLowerCase() },
		(err, dupe) =>
		{
			if(err)
			{
				res.status(500).send({ 
				message: 'Error consultando la BD'});
			} else {

				if (!dupe)
				{
					user.name = params.name;
					user.surname = params.surname;
					user.email = params.email.toLowerCase();
					user.role = params.role;
					user.image = null;

					bcrypt.hash(params.password, null, null, (err, hash) =>
					{
						user.password = hash;
						user.save((err, data) =>
						{
							if (err)
							{
								res.status(500).send({ 
								message: 'Error al guardar el dato' });
							} else {

								if (!data)
								{
									res.status(400).send({ 
									message: 'No se puede registrar el usuario' });
								} else {

									res.status(200).send({ user: data });
								}
							}
						});
					});
				} else {

					res.status(500).send({ 
					message: 'Email de usuario ya registrado' });
				}
			}
		});
	} else {

		res.status(400).send({ 
		message: 'No se han recibido datos'});
	}
}

function read(req, res)
{
	res.status(200).send({ message: 'Leer usuario'	});
}

function update(req, res)
{
	let userId = req.params.id;
	let update = req.body;

	if(update.password)
	{
		bcrypt.hash(update.password, null, null, (err, hash) =>
		{
			if(err)
			{
				delete update.password;
				console.log('No se pudo actualizar la contraseña');
				saveData();
			} else {

				update.password = hash;
				console.log('password encrypted');
				saveData();
			}
		});
	} else {

		delete update.password;
		console.log('falta contraseña');
		saveData();
	}

	function saveData()
	{
		User.findByIdAndUpdate(userId, update, {new: false}, (err, user) =>
		{
			if(err)
			{
				res.status(500).send({ 
				message: 'No puede actualizar usuario' });
			} else {

				if(user)
				{
					res.status(200).send({ user: user });
				} else {

					res.status(404).send({ 
					message: 'No puede encontrar usuario' });
				}
			}
		});
	}
}

function destroy(req, res)
{
	let userId = req.params.id;

	User.findByIdAndRemove(userId, (err, user) =>
	{
		if(err)
		{
			res.status(500).send({ message: 'No puede borrar usuario' });
		} else {

			if(user)
			{
				res.status(200).send({ user });
			} else {

				res.status(404).send({ message: 'No puede encontrar usuario' });
			}
		}
	});
}

function list(req, res)
{
	User.find({}).exec((err, users) =>
	{
		if(err)
		{
			res.status(500).send({ message: 'Get error' });
		} else {

			if(users)
			{
				res.status(200).send({ users });
			} else {

				res.status(200).send({ message: 'No se han encontrado usuarios' });
			}
		}
	});
}

module.exports = { create, read, update, destroy, list };