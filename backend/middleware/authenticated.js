'use strict';

let secret = require('../globals').secret;

let jwt = require('jwt-simple');
let moment = require('moment');

exports.ensureAuth = function(req, res, next)
{
	if(!req.headers.authorization)
	{
		return res.status(403).send({ 
		message: 'Falta cabecera de authorization'});
	}

	let token = req.headers.authorization.replace(/['"]+/g, '');
	let payload = jwt.decode(token, secret);

	try
	{
		if(payload.exp <= moment().unix())
		{
			return res.status(401).send({ 
			message: 'La autorización ha expirado'});
		}
	} catch(ex) {

		return res.status(404).send({ 
		message: 'Auhtorizacion invalida'});
	}

	req.user = payload;

	next();
};
